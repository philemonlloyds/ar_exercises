require_relative '../setup'
require_relative './exercise_1'
require_relative './exercise_2'
require_relative './exercise_3'
require_relative './exercise_4'

puts "Exercise 5"
puts "----------"

# Your code goes here ...

# Person.sum(:age)
total_revenue = Store.sum(:annual_revenue)
puts "total_revenue = #{total_revenue}"

avg_annual_revenue = Store.average(:annual_revenue)
puts "avg_annual_revenue = #{avg_annual_revenue}"

stores_annual_sales_1m = Store.where("annual_revenue>?",1_000_000).count
puts "Number of stores where sales is greater than 1M = #{stores_annual_sales_1m}"