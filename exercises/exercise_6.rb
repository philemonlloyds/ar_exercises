require_relative '../setup'
require_relative './exercise_1'
require_relative './exercise_2'
require_relative './exercise_3'
require_relative './exercise_4'
require_relative './exercise_5'

puts "Exercise 6"
puts "----------"

# Your code goes here ...

@store1.employees.create(first_name: "Kobe", last_name: "Bryant", hourly_rate: 600)
@store1.employees.create(first_name: "Lebron", last_name: "James", hourly_rate: 500)
@store1.employees.create(first_name: "Stephen", last_name: "Curry", hourly_rate: 450)

@store2.employees.create(first_name: "Draymond", last_name: "Green", hourly_rate: 100)
@store2.employees.create(first_name: "Matthew", last_name: "Dellavadova", hourly_rate: 50)
@store2.employees.create(first_name: "Klay", last_name: "Thompson", hourly_rate: 250)