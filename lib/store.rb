class Store < ActiveRecord::Base
  has_many  :employees
  validates :name, presence: true , length: { minimum: 3 }
  validates :annual_revenue, numericality: { only_integer: true,greater_than: 0}
  validate  :must_be_mens_or_women , on: :create
  validate  :must_be_mens_or_women_false , on: :create


def must_be_mens_or_women
  if mens_apparel == nil &&  womens_apparel == nil
    errors.add(:base, "Store must have women or men's apparel")
  end
end

def must_be_mens_or_women_false
  if mens_apparel == false &&  womens_apparel == false
    errors.add(:base, "Store must have women or men's apparel - Both Women and Men are missing")
  end
end

end
